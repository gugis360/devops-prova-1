<?php

namespace Mova\DevOpsExample\Test\Unit\Service;

use DateTime;
use DateTimeZone;
use Mova\DevOpsExample\Service\TimeFormatter;
use PHPUnit\Framework\TestCase;

class TimeFormatterTest extends TestCase
{
    /**
     * @dataProvider timezoneProvider
     */
    public function testInTimezone(DateTime $dt, DateTimeZone $dtz, string $expected): void
    {
        $timeFormatter = $this->createTimeFormatterInstance();

        $this->assertEquals($expected, $timeFormatter->inTimezone($dt, $dtz));
    }

    public function timezoneProvider(): array
    {
        $dt1 = new DateTime('2022-07-04T11:29:33+0000');
        return [
            'Correct time in New York' => [
                $dt1,
                new DateTimeZone('America/New_York'),
                '2022-07-04T07:29:33-0400',
            ],
            'Correct time in Bangkok' => [
                $dt1,
                new DateTimeZone('Asia/Bangkok'),
                '2022-07-04T18:29:33+0700',
            ],
            'Correct time in Tokyo' => [
                $dt1,
                new DateTimeZone('Asia/Tokyo'),
                '2022-07-04T20:29:33+0900',
            ],
            'Correct time in Casey Antarctica' => [
                $dt1,
                new DateTimeZone('Antarctica/Casey'),
                '2022-07-04T22:29:33+1100',
            ],
            'Correct time in Brazil' => [
                $dt1,
                new DateTimeZone('America/Sao_Paulo'),
                '2022-07-04T08:29:33-0300',
            ],
        ];
    }

    private function createTimeFormatterInstance(): TimeFormatter
    {
        return new TimeFormatter();
    }
}
